﻿using System;

namespace Task03
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public override string ToString()
        {
            return $"Имя: {FirstName}, фамилия: {LastName}, дата рождения: {BirthDate}.";
        }
    }
}
