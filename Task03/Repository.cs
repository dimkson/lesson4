﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task03
{
    public class Repository<T> : IRepository<T>
    {
        private IList<T> _repo;

        public Repository(IEnumerable<T> repo)
        {
            _repo = repo.ToList();
        }

        public void Add(T item)
        {
            _repo.Add(item);
        }

        public IEnumerable<T> GetAll()
        {
            for (int i = 0; i < _repo.Count; i++)
            {
                yield return _repo[i];
            }
        }

        public T GetOne(Func<T, bool> predicate)
        {
            return _repo.Where(predicate).First();
        }
    }
}
