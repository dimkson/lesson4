﻿namespace Task03
{
    public class AccountService : IAccountService
    {
        private IRepository<Account> _repository;

        public AccountService(IRepository<Account> repository)
        {
            _repository = repository;
        }
        public bool AddAccount(Account account)
        {
            if (account.FirstName != string.Empty)
                if (account.LastName != string.Empty)
                    {
                        _repository.Add(account);
                        return true;
                    }
            return false;
        }
    }
}
