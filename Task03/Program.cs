﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task03
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Account> accounts = new List<Account>()
            {
                new Account { FirstName="Иван", LastName="Иванов", BirthDate=DateTime.Now },
                new Account { FirstName="Петр", LastName="Петров", BirthDate=DateTime.Now }
            };

            Repository<Account> repository = new Repository<Account>(accounts);

            foreach (var item in repository.GetAll())
                Console.WriteLine(item);

            var account = repository.GetOne(ac => ac.LastName == "Петров");
            Console.WriteLine(account);

            accounts.Add(account);

            Console.ReadLine();
        }
    }
}
