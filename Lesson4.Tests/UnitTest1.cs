using System;
using Xunit;
using Task03;
using System.Collections.Generic;
using Moq;

namespace Lesson4.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(repo => repo.GetAll()).Returns(GetData());

            AccountService service = new AccountService(mock.Object);

            Assert.NotNull(service);
            Assert.True(service.AddAccount(new Account { LastName = "34", FirstName="234", BirthDate=DateTime.Now }));
            Assert.False(service.AddAccount(new Account { FirstName = "" }));
            Assert.IsType<List<Account>>(mock.Object.GetAll());
        }
        private IEnumerable<Account> GetData()
        {
            return new List<Account>
            {
                new Account { FirstName="����", LastName="������", BirthDate=DateTime.Now },
                new Account { FirstName="����", LastName="������", BirthDate=DateTime.Now }
            };
        }
    }
}
