﻿using System;
using System.Collections.Generic;

namespace Lesson4
{
    class Program
    {
        private const string PATH = "text.txt";

        static void Main()
        {
            XmlSerializer<Person> serializer = new XmlSerializer<Person>();

            FileManager<Person> fm = new FileManager<Person>(PATH, serializer);

            People<Person> people = new People<Person>(fm.Load());

            //Если коллекция пуста, заполняем пустой файл случайными данными и считываем их в коллекцию
            if (people.Count == 0)
            {
                Fill(fm);
                people = new People<Person>(fm.Load());
            }

            foreach (var item in people)
                Console.WriteLine(item);
            
            people.Sort();

            Console.WriteLine();
            foreach (var item in people)
                Console.WriteLine(item);
            
            Console.ReadLine();
        }

        /// <summary>
        /// Метод заполняет файл случайными данными.
        /// </summary>
        static void Fill(IFileManager<Person> fileManager)
        {
            List<Person> list = new List<Person>();
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                list.Add(new Person { Name = $"Person:{i}", Age = rnd.Next(1, 100), Surname = $"Surname:{i}" });
            }
            fileManager.Save(list);
            Console.WriteLine("Новый файл сгенерирован.");
        }
    }
}
