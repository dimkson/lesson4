﻿using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;

namespace Lesson4
{
    class XmlSerializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings xmlWriterSettings;

        static XmlSerializer()
        {
            xmlWriterSettings = new XmlWriterSettings { Indent = true };
        }

        public T Deserialize<T>(Stream data)
        {
            IExtendedXmlSerializer deserializer = new ConfigurationContainer().Create();
            return deserializer.Deserialize<T>(data);
        }

        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
                .UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(T))
                .Create();
            return serializer.Serialize(xmlWriterSettings, item);
        }
    }
}
