﻿using System;

namespace Lesson4
{
    class Person: IComparable<Person>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        public int CompareTo(Person other)
        {
            return Age - other.Age;
        }

        public override string ToString()
        {
            return $"Name:{Name}, Surname:{Surname}, Age:{Age}";
        }
    }
}
