﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace Lesson4
{
    /// <summary>
    /// Класс представляет из себя коллекцию с возможностью сортировки.
    /// </summary>
    /// <typeparam name="T">Любой тип, реализующий интерфейс IComparable<T></typeparam>
    class People<T> : IEnumerable<T>, IAlgorithm where T : IComparable<T>
    {
        private T[] _arr;

        public int Count => _arr.Length;

        public People(IEnumerable<T> arr)
        {
            _arr = arr.ToArray();
        }

        // Реализация именованного итератора.
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < _arr.Length; i++)
            {
                yield return _arr[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _arr.GetEnumerator();
        }

        // Метод сортирует массив
        public void Sort()
        {
            _arr = _arr.OrderBy(e => e).ToArray();
        }
    }
}
