﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Lesson4
{
    interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream data);
    }
}
