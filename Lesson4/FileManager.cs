﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lesson4
{
    class FileManager<T> : IFileManager<T>
    {
        private string _path;
        private ISerializer<T> _serializer;

        public FileManager(string path, ISerializer<T> serializer)
        {
            _path = path;
            _serializer = serializer;
        }

        /// <summary>
        /// Метод открывает файл на чтение, десериализует его и возвращает IEnumerable.
        /// </summary>
        public IEnumerable<T> Load()
        {
            try
            {
                using (FileStream fs = new FileStream(_path, FileMode.Open, FileAccess.Read))
                {
                    return _serializer.Deserialize<T[]>(fs);
                }
            }
            catch (FileNotFoundException exc)
            {
                Console.WriteLine(exc.Message);
                return Array.Empty<T>();
            }
        }

        /// <summary>
        /// Метод сериализует коллекцию и сохраняет её в файл.
        /// </summary>
        public void Save(IEnumerable<T> list)
        {
            using (FileStream fs = new FileStream(_path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.Write(_serializer.Serialize<T[]>(list.ToArray()));
                }
            }
        }
    }
}
