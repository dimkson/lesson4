﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    interface IFileManager<T>
    {
        void Save(IEnumerable<T> list);
        IEnumerable<T> Load();
    }
}
